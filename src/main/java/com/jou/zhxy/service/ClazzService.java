package com.jou.zhxy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jou.zhxy.pojo.Admin;
import com.jou.zhxy.pojo.Clazz;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 18:34
 */


public interface ClazzService extends IService<Clazz> {
    IPage getClazzByOpr(Page<Clazz> page, Clazz clazz);
}
