package com.jou.zhxy.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jou.zhxy.pojo.LoginForm;
import com.jou.zhxy.pojo.Student;
import com.jou.zhxy.pojo.Teacher;

public interface StudentService extends IService<Student> {


    Student login(LoginForm loginForm);

    Student getStudentById(Long userId);

    IPage<Student> getStudentByOpr(Page<Student> pageParam, Student student);

}
