package com.jou.zhxy.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 **/


/**
 * 方式一：实现WebMvcConfigurer接口，重写addCorsMappings()方法
 * 解决跨域问题
 **/

@EnableWebMvc
@Configuration
public class CORSConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/*")
            .allowedOrigins("*")
            .allowedHeaders("*")
            .allowedMethods("*");

//配置
    }
}
