package com.jou.zhxy.config;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2023-03-16 22:41
 * @project IntelliJ IDEA
 **/

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 方式二：返回新的CorsFilter
 * 解决跨域问题
 *
 * **/
@Configuration
public class CorsFilterConfig {

    @Bean
    public CorsFilter corsFilter(){
        //1. 添加 CORS配置信息
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedHeader("*"); //放行哪些原始请求头部信息
        corsConfiguration.addAllowedMethod("*"); //放行哪些请求方式
        corsConfiguration.addAllowedOrigin("*"); //放行哪些原始域
        corsConfiguration.addExposedHeader("*"); //暴露哪些头部信息

        //2. 添加映射路径
        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        corsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);

        //3. 返回新的CorsFilter
        return new CorsFilter(corsConfigurationSource);

    }

}
