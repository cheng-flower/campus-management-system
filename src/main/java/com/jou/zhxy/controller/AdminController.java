package com.jou.zhxy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jou.zhxy.pojo.Admin;
import com.jou.zhxy.service.AdminService;
import com.jou.zhxy.util.MD5;
import com.jou.zhxy.util.Result;
import com.sun.org.apache.regexp.internal.RE;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 19:09
 **/
@Api(tags = "管理员控制器")
@RestController
@RequestMapping("/sms/adminController")
public class AdminController {

    @Autowired
    private AdminService adminService;


    //  GET
    //	http://localhost:9002/sms/adminController/getAllAdmin/1/3?adminName=
    @ApiOperation("分页带条件查询管理员信息")
    @GetMapping("/getAllAdmin/{pageNo}/{pageSize}")
    public Result getAllAdmin(@PathVariable("pageNo") Integer pageNo,
                              @PathVariable("pageSize") Integer pageSize,
                              String adminName) {

        Page<Admin> page = new Page<>(pageNo, pageSize);

        IPage<Admin> iPage = adminService.getAdminsByOpr(page, adminName);

        return Result.ok(iPage);
    }


    // POST
    //	http://localhost:9002/sms/adminController/saveOrUpdateAdmin  admin
    @ApiOperation("新增或修改管理员信息")
    @PostMapping("/saveOrUpdateAdmin")
    public Result saveOrUpdateAdmin(@RequestBody Admin admin) {

        Integer id = admin.getId();

        if (null == id || 0 == id) {
            // 没有id是新增，有id是修改，修改时需要将网页明文密码进行加密后保存到数据库
            admin.setPassword(MD5.encrypt(admin.getPassword()));
        }
        adminService.saveOrUpdate(admin);

        return Result.ok();

    }


    // DELETE
    //	http://localhost:9002/sms/adminController/deleteAdmin   参数:List<Integer> ids

    @ApiOperation("删除单个或多个管理员信息")
    @DeleteMapping("/deleteAdmin")
    public Result deleteAdmin(@RequestBody List<Integer> ids) {

        adminService.removeByIds(ids);

        return Result.ok();

    }


}
