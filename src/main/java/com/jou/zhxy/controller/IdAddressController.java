package com.jou.zhxy.controller;

import com.baidubce.http.ApiExplorerClient;
import com.baidubce.http.AppSigner;
import com.baidubce.http.HttpMethodName;
import com.baidubce.model.ApiExplorerRequest;
import com.baidubce.model.ApiExplorerResponse;

import java.util.UUID;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-08-02 22:52
 */


public class IdAddressController {

    // 身份证归属地查询 示例代码
    public static void main(String[] args) {
        String path = "http://qryidcard.api.bdymkt.com/lundear/qryidcard";
        ApiExplorerRequest request = new ApiExplorerRequest(HttpMethodName.GET, path);
        request.setCredentials("16cdf3c241e54e2f8da07470e3ef8398", "93e406de8b5442be8b62093c1cdb790a");

        // 设置header参数
        request.addHeaderParameter("Content-Type", "application/json; charset=utf-8");

        // 设置query参数
        request.addQueryParameter("idcard", "450422199706161135");


        ApiExplorerClient client = new ApiExplorerClient(new AppSigner());

        try {
            ApiExplorerResponse response = client.sendRequest(request);
            // 返回结果格式为Json字符串
            System.out.println(response.getResult());
            String s = UUID.randomUUID().toString();
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


