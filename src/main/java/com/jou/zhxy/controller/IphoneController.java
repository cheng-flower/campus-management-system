package com.jou.zhxy.controller;


import com.baidubce.http.ApiExplorerClient;
import com.baidubce.http.AppSigner;
import com.baidubce.http.HttpMethodName;
import com.baidubce.model.ApiExplorerRequest;
import com.baidubce.model.ApiExplorerResponse;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-08-01 22:10
 */

public class IphoneController {

    public static void main(String[] args) {
        //号码归属查询api Java示例代码
        // http://gsd.api.bdymkt.com/sms
        String path = "http://gsd.api.bdymkt.com/sms";
        ApiExplorerRequest request = new ApiExplorerRequest(HttpMethodName.POST, path);
        request.setCredentials("16cdf3c241e54e2f8da07470e3ef8398", "93e406de8b5442be8b62093c1cdb790a");


        request.addHeaderParameter("Content-Type", "application/json;charset=UTF-8");
//        request.addHeaderParameter("X-Bce-Signature", "c44dc2e5fade4d988517ac1d477f1796 ");
        request.addQueryParameter("mobile", "13038852045");

        ApiExplorerClient client = new ApiExplorerClient(new AppSigner());

        try {
            ApiExplorerResponse response = client.sendRequest(request);
            // 返回结果格式为Json字符串
            System.out.println(response.getResult());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
