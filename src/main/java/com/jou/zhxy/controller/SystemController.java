package com.jou.zhxy.controller;

import com.jou.zhxy.pojo.Admin;
import com.jou.zhxy.pojo.LoginForm;
import com.jou.zhxy.pojo.Student;
import com.jou.zhxy.pojo.Teacher;
import com.jou.zhxy.service.AdminService;
import com.jou.zhxy.service.StudentService;
import com.jou.zhxy.service.TeacherService;
import com.jou.zhxy.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 17:14
 */

@Api(tags = "系统控制器")
@RestController
@RequestMapping("sms/system")
public class SystemController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private TeacherService teacherService;

    /*
        * 修改密码的处理器
        * POST  /sms/system/updatePwd/123456/admin
        *       /sms/system/updatePwd/{oldPwd}/{newPwd}
        *       请求参数
                    oldpwd
                    newPwd
                    token 请求头
                响应的数据
                    Result OK data= null
     **/


    @ApiOperation("更新用户密码的处理器")
    @PostMapping("/updatePwd/{oldPwd}/{newPwd}")
    public Result updatePwd(@RequestHeader("token") String token,
                            @PathVariable("oldPwd") String oldPwd,
                            @PathVariable("newPwd") String newPwd) {

        boolean expiration = JwtHelper.isExpiration(token);
        if (expiration) {
            //token过期
            return Result.fail().message("token过期,请重新登录");
        }
        //获取用户ID和用户类型
        Long userId = JwtHelper.getUserId(token);
        Integer userType = JwtHelper.getUserType(token);

        oldPwd = MD5.encrypt(oldPwd);
        newPwd = MD5.encrypt(newPwd);

        switch (userType) {

            case 1:
                Admin admin = adminService.getAdminById(userId);
                if (admin != null) {
                    admin.setPassword(newPwd);
                    adminService.saveOrUpdate(admin);
                } else {
                    return Result.fail().message("原密码有误！");
                }
                break;
            case 2:
                Student student = studentService.getStudentById(userId);
                if (student != null) {
                    student.setPassword(newPwd);
                    studentService.saveOrUpdate(student);
                } else {
                    return Result.fail().message("原密码有误！");
                }
                break;
            case 3:
                Teacher teacher = teacherService.getTeacherById(userId);
                if (teacher != null) {
                    teacher.setPassword(newPwd);
                    teacherService.saveOrUpdate(teacher);
                } else {
                    return Result.fail().message("原密码有误！");
                }
                break;
            default:
                ;

        }


        return Result.ok();
    }


    // POST /sms/system/headerImgUpload
    @ApiOperation("文件上传同意入口")
    @PostMapping("/headerImgUpload")
    public Result headerImgUpload(@RequestPart("multipartFile") MultipartFile multipartFile) {

        String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
        String originalFilename = multipartFile.getOriginalFilename();
        int index = originalFilename.lastIndexOf(".");
        //substring:截取字符串
        String newFileName = uuid.concat(originalFilename.substring(index));

        //保存文件

        String portraitPath = "D:/Java教程/Springboot_workplace002/zhxy/target/classes/public/upload".concat(newFileName);
        try {
            multipartFile.transferTo(new File(portraitPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //响应图片的路径
        String path = "upload/".concat(newFileName);
        return Result.ok(path);
    }


    @GetMapping("/getInfo")
    public Result getInfoByToken(@RequestHeader("token") String token) {

        boolean isExpiration = JwtHelper.isExpiration(token);
        if (isExpiration) {
            return Result.build(null, ResultCodeEnum.TOKEN_ERROR);
        }

        Map<String, Object> map = new LinkedHashMap<>();

        //从token中获取用户ID和用户类型
        Long userId = JwtHelper.getUserId(token);
        Integer userType = JwtHelper.getUserType(token);

        switch (userType) {
            case 1:
                Admin admin = adminService.getAdminById(userId);
                map.put("userType", 1);
                map.put("user", admin);
                break;
            case 2:
                Student student = studentService.getStudentById(userId);
                map.put("userType", 2);
                map.put("user", student);
                break;
            case 3:
                Teacher teacher = teacherService.getTeacherById(userId);
                map.put("userType", 3);
                map.put("user", teacher);
                break;
        }

        return Result.ok(map);
    }

    @ApiOperation("获取验证码图片")
    @GetMapping("/getVerifiCodeImage")
    public void getVerifiCodeImage(HttpServletRequest request, HttpServletResponse response) {

        //获取图片
        BufferedImage verifiCodeImage = CreateVerifiCodeImage.getVerifiCodeImage();
        //获取图片上的验证码
        String verifiCode = new String(CreateVerifiCodeImage.getVerifiCode());
        //将验证码文本存入session域中，供下次使用
        HttpSession session = request.getSession();
        session.setAttribute("verifiCode", verifiCode);
        //将验证码图片响应给浏览器

        ServletOutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            ImageIO.write(verifiCodeImage, "JPEG", outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @ApiOperation("登录控制器")
    @PostMapping("/login")
    public Result login(HttpServletRequest request, @RequestBody LoginForm loginForm) {

        //验证码校验
        String sessionVerifiCode = (String) request.getSession().getAttribute("verifiCode");
        String loginVerifiCode = loginForm.getVerifiCode();

        if ("".equals(sessionVerifiCode) || null == sessionVerifiCode) {
            return Result.fail().message("验证码失效，请刷新后重试");
        }
        if (!sessionVerifiCode.equalsIgnoreCase(loginVerifiCode)) {
            return Result.fail().message("验证码有误，请重新输入");
        }
        //从session域中移除现有验证码
        request.getSession().removeAttribute("verifiCode");

        Map map = new LinkedHashMap();

        //分用户类型进行校验
        Integer userType = loginForm.getUserType();
        switch (userType) {
            case 1:
                try {
                    Admin admin = adminService.login(loginForm);
                    if (null != admin) {
                        String token = JwtHelper.createToken(admin.getId().longValue(), 1);
                        map.put("token", token);
                    } else {
                        throw new RuntimeException("用户名或密码错误");
                    }
                    return Result.ok(map);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    return Result.fail().message(e.getMessage());
                }

            case 2:
                try {
                    Student student = studentService.login(loginForm);
                    if (null != student) {
                        String token = JwtHelper.createToken(student.getId().longValue(), 2);
                        map.put("token", token);
                    } else {
                        throw new RuntimeException("用户名或密码错误");
                    }
                    return Result.ok(map);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    return Result.fail().message(e.getMessage());
                }

            case 3:
                try {
                    Teacher teacher = teacherService.login(loginForm);
                    if (null != teacher) {
                        String token = JwtHelper.createToken(teacher.getId().longValue(), 3);
                        map.put("token", token);
                    } else {
                        throw new RuntimeException("用户名或密码错误");
                    }
                    return Result.ok(map);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    return Result.fail().message(e.getMessage());
                }


        }

        return Result.fail().message("查无此用户");
    }


}
