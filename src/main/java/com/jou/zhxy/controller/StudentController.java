package com.jou.zhxy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jou.zhxy.pojo.Student;
import com.jou.zhxy.service.StudentService;
import com.jou.zhxy.util.MD5;
import com.jou.zhxy.util.Result;
import com.sun.corba.se.spi.activation._RepositoryStub;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.List;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 19:09
 */

@Api(tags = "学生控制器")
@RestController
@RequestMapping("/sms/studentController")
public class StudentController {

    @Autowired
    private StudentService studentService;


    //GET /sms/studentController/getStudentByOpr/1/3?name=&clazzName=
    @ApiOperation("分页带条件查询学生信息")
    @GetMapping("/getStudentByOpr/{pageNo}/{pageSize}")
    public Result getStudentByOpr(
        @ApiParam("分页查询的页码数") @PathVariable("pageNo") Integer pageNo,
        @ApiParam("分页查询的页大小") @PathVariable("pageSize") Integer pageSize,
        @ApiParam("查询的条件") Student student
    ) {

        Page<Student> pageParam = new Page<>(pageNo, pageSize);

        //进行查询
        IPage<Student> studentIPage = studentService.getStudentByOpr(pageParam, student);
        //封装Result返回
        return Result.ok(studentIPage);
    }


    // POST  http://localhost:9002/sms/studentController/addOrUpdateStudent
    @ApiOperation("新增或修改学生信息")
    @PostMapping("/addOrUpdateStudent")
    public Result addOrUpdateStudent(@ApiParam("新增或修改的学生信息") @RequestBody Student student) {

        Integer id = student.getId();
        if (null == id || 0 == id) {
            student.setPassword(MD5.encrypt(student.getPassword()));
        }
        studentService.saveOrUpdate(student);

        return Result.ok();
    }


    //sms/studentController/delStudentById
    @ApiOperation("删除学生信息")
    @DeleteMapping("/delStudentById")
    public Result delStudentById(@ApiParam("要删除的学生编号的JSON数组") @RequestBody List<Integer> ids) {

        studentService.removeByIds(ids);

        return Result.ok();
    }
}
