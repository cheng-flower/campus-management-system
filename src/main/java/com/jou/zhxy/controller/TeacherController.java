package com.jou.zhxy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jou.zhxy.pojo.Teacher;
import com.jou.zhxy.service.TeacherService;
import com.jou.zhxy.util.MD5;
import com.jou.zhxy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 19:09
 */

@Api(tags = "教师控制器")
@RestController
@RequestMapping("/sms/teacherController")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    /*
     * get  sms/teacherController/getTeachers/1/3
     *      sms/teacherController/getTeachers/1/3?name=小&clazzName=一年一班
     *      请求数据
     *      响应Result  data= 分页
     * */
    @ApiOperation("分页带条件查询教师信息")
    @GetMapping("/getTeachers/{pageNo}/{pageSize}")
    public Result getTeachers(@ApiParam("分页当前页") @PathVariable("pageNo") Integer pageNo,
                              @ApiParam("分页大小") @PathVariable("pageSize") Integer pageSize,
                              Teacher teacher) {

        Page<Teacher> page = new Page<>(pageNo, pageSize);
        IPage<Teacher> teacherIPage = teacherService.getTeachersByOpr(page, teacher);

        return Result.ok(teacherIPage);
    }


    /*
     * post sms/teacherController/saveOrUpdateTeacher
     *      请求数据 Teacher
     *      响应Result data= null OK
     * */
    @ApiOperation("新增或修改教师信息")
    @PostMapping("/saveOrUpdateTeacher")
    public Result saveOrUpdateTeacher(@RequestBody Teacher teacher) {

        // 如果是新增,要对密码进行加密
        if (null == teacher.getId() || 0 == teacher.getId()) {
            teacher.setPassword(MD5.encrypt(teacher.getPassword()));
        }

        teacherService.saveOrUpdate(teacher);

        return Result.ok();
    }


    /*
     * DELETE sms/teacherController/deleteTeacher
     *   请求的数据 JSON 数组 [1,2,3]
     *   响应Result data =null  OK
     *
     * */
    @ApiOperation("删除单个或多个教师信息")
    @DeleteMapping("/deleteTeacher")
    public Result deleteTeacher(@RequestBody List<Integer> ids) {

        teacherService.removeByIds(ids);

        return Result.ok();

    }


}
