package com.jou.zhxy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jou.zhxy.pojo.Clazz;
import com.jou.zhxy.service.ClazzService;
import com.jou.zhxy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 19:09
 **/
@Api(tags = "班级控制器")
@RestController
@RequestMapping("/sms/clazzController")
public class ClazzController {

    @Autowired
    private ClazzService clazzService;

    @ApiOperation("分页带条件查询班级信息")
    @GetMapping("/getClazzsByOpr/{pageNo}/{pageSize}")
    public Result getClazzByOpr(@ApiParam("分页查询的页码数") @PathVariable("pageNo") Integer pageNo,
                                @ApiParam("分页查询的页大小") @PathVariable("pageSize") Integer pageSize,
                                @ApiParam("分页查询的条件") Clazz clazz) {
        Page<Clazz> page = new Page<>(pageNo, pageSize);

        IPage iPage = clazzService.getClazzByOpr(page, clazz);

        return Result.ok(iPage);

    }


    @ApiOperation("新增或修改班级信息")
    @PostMapping("/saveOrUpdateClazz")
    public Result saveOrUpdateClazz(@ApiParam("JSON格式的班级信息") @RequestBody Clazz clazz) {
        clazzService.saveOrUpdate(clazz);
        return Result.ok();
    }


    @ApiOperation("删除单个或多个班级")
    @DeleteMapping("/deleteClazz")
    public Result deleteClazz(@ApiParam("要删除的班级id的json集合") @RequestBody List<Integer> ids) {
        clazzService.removeByIds(ids);
        return Result.ok();
    }


    @ApiOperation("获取所有班级信息")
    @GetMapping("/getClazzs")
    public Result getClazzs() {
        List<Clazz> clazzes = clazzService.list();
        return Result.ok(clazzes);
    }


}
