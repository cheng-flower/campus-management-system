package com.jou.zhxy.controller;

import com.baidubce.http.ApiExplorerClient;
import com.baidubce.http.AppSigner;
import com.baidubce.http.HttpMethodName;
import com.baidubce.model.ApiExplorerRequest;
import com.baidubce.model.ApiExplorerResponse;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-08-02 22:52
 */


public class IDController {

    // 身份证二要素 Java示例代码
    public static void main(String[] args) {

        String path = "http://dfidname.api.bdymkt.com/verify_id_name";
        ApiExplorerRequest request = new ApiExplorerRequest(HttpMethodName.POST, path);
        request.setCredentials("16cdf3c241e54e2f8da07470e3ef8398", "93e406de8b5442be8b62093c1cdb790a");

        request.addHeaderParameter("Content-Type", "application/x-www-form-urlencoded");

        String requestExample = "id_number=450422199706161135&name=唐海钦";
        request.setJsonBody(requestExample);

        ApiExplorerClient client = new ApiExplorerClient(new AppSigner());

        try {
            ApiExplorerResponse response = client.sendRequest(request);
            // 返回结果格式为Json字符串
            System.out.println(response.getResult());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

