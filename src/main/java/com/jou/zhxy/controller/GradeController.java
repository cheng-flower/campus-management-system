package com.jou.zhxy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jou.zhxy.pojo.Grade;
import com.jou.zhxy.service.GradeService;
import com.jou.zhxy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 19:09
 */

@Api(tags = "年级控制器")
@RestController
@RequestMapping("/sms/gradeController")
public class GradeController {

    @Autowired
    private GradeService gradeService;


    @ApiOperation("获取全部年级")
    @GetMapping("/getGrades")
    public Result getGrades() {
        List<Grade> list = gradeService.list();
        return Result.ok(list);
    }

    @ApiOperation("删除grade信息")
    @DeleteMapping("/deleteGrade")
    public Result deleteGrade(
        @ApiParam("要删除的所有grade的id的JSON集合") @RequestBody List<Integer> ids
    ) {

        gradeService.removeByIds(ids);
        return Result.ok();
    }

    @ApiOperation("新增或修改grade,有id是修改，没有是新增")
    @PostMapping("/saveOrUpdateGrade")
    public Result saveOrUpdateGrade(
        @ApiParam("JSON的Grade对象") @RequestBody Grade grade) {

        gradeService.saveOrUpdate(grade);
        return Result.ok();
    }


    //sms/gradeController/getGrades/1/3?gradeName=%E4%B8%89
    @ApiOperation("根据年级名称模糊查询，带分页")
    @GetMapping("/getGrades/{pageNo}/{pageSize}")
    public Result getGrades(@ApiParam("分页查询的页码数") @PathVariable("pageNo") Integer pageNo,
                            @ApiParam("分页查询的页大小") @PathVariable("pageSize") Integer pageSize,
                            @ApiParam("分页模糊查询的匹配名称") String gradeName) {

        //分页查询
        Page<Grade> pageParam = new Page<>(pageNo, pageSize);

        //通过服务层进行分页查询
        IPage<Grade> pageRs = gradeService.getGradeByOpr(pageParam, gradeName);


        // 封装Result对象并返回
        return Result.ok(pageRs);
    }

}
