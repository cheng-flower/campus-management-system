package com.jou.zhxy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jou.zhxy.pojo.Grade;
import org.springframework.stereotype.Repository;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 18:32
 */

@Repository
public interface GradeMapper extends BaseMapper<Grade> {
}
