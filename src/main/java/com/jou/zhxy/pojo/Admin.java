package com.jou.zhxy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 18:08
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_admin") //绑定至指定的数据表
public class Admin {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Character gender;
    private String password;
    private String email;
    private String telephone;
    private String address;
    private String portraitPath;  //头像的图片位置


}
