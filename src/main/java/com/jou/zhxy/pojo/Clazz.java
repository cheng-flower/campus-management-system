package com.jou.zhxy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sun.org.apache.xpath.internal.operations.Or;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2022-06-27 18:13
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_clazz") //绑定至指定的数据表
public class Clazz {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer number;
    private String introduction;
    private String headmaster;
    private String email;
    private String telephone;
    private String gradeName;

}
