package com.jou.zhxy;

import com.jou.zhxy.pojo.Admin;
import com.jou.zhxy.service.AdminService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.zip.DeflaterOutputStream;

@SpringBootTest
class ZhxyApplicationTests {

    @Autowired
    private AdminService adminService;

    @Test
    void contextLoads() {
//        Admin byId = adminService.getById(101L);
//        System.out.println(byId);
        List<Admin> list = adminService.list();
        for (Admin admin : list) {
            System.out.println(admin);
        }

    }

}
